const DEBUG = false;

var Masto = require('mastodon');
require('dotenv').config();
const mensaar = require('./mensaarformatter/lib.js');

const M = new Masto({
  access_token: process.env.TOKEN,
  api_url: `${process.env.BASE}api/v1/`,
});


const post = (x, resp) => {
    let query = {"status" : x};
    if (resp !== undefined) {
        query["in_reply_to_id"] = resp.data.id;
    }
    if (DEBUG) {
        console.log(x, resp);
        return Promise.resolve({ data: { id :42},});
    }
    return M.post('statuses', query);
}

const printMenu = (data) => {
    let promise = data.reduce((a,x) => a.then(resp => post(x, resp), console.log), Promise.resolve(undefined));
    promise.then(x => console.log("Success!"), console.log);
}

mensaar.mensamenu(0, process.env.MENSA_KEY, x=>x).then(printMenu, console.log);
