# A Mensaar Bot for Mastodon

## Setup:

### Dependencies :

```
git clone https://gitlab.com/samsumas/mastomensaardon
cd mastomensaardon

#maybe there is a faster way to use submodules...
git submodule update --init

npm install
```

Then create an .env with

```
TOKEN='enter_masto_token_here'
BASE='https://botsin.space/_or_whatever_your_favorite_instance_is'
MENSA_KEY='enter_apikey_for_mensaar.de'

```

## Usage:

The bot will TOOT todays menu (except on days when the mensa is closed : it justs prints the menu of the next opened day) and exits.

`node bot.js`

### Cool kids use Cron

```cron
#minute         hour            day_of_month    month   day_of_week     command
00              9               *               *       1-5             cd /path/to/mastomensaardon/ && { ./run.sh >> ./run.log ;}
00              11              *               *       1-5             cd /path/to/mastomensaardon/ && { ./run.sh >> ./run.log ;}

```
